import { BaseEntity, Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

import { ITask } from './task.interface'
import { TaskStatus } from './task-status.enum'

@Entity()
export class Task extends BaseEntity implements ITask {
  @PrimaryGeneratedColumn('uuid')
  taskId!: string

  @Column({
    nullable: false,
    length: 100,
  })
  title!: string

  @Column({
    nullable: false,
    default: '',
  })
  description!: string

  @Column({
    nullable: false,
    type: 'enum',
    enum: TaskStatus,
    default: TaskStatus.OPEN,
  })
  status!: TaskStatus
}
