import { TaskStatus } from './task-status.enum'

export interface ITask {
  readonly taskId: string
  title: string
  description: string
  status: TaskStatus
}
