import { Injectable, NotFoundException } from '@nestjs/common'

// import { ITask } from './task.interface'
import { CreateTaskDto } from './dto/create-task.dto'
import { UpdateTaskDto } from './dto/update-task.dto'
import { InjectRepository } from '@nestjs/typeorm'
import { TaskRepository } from './task.repository'
import { Task } from './task.entity'

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository) private taskRepository: TaskRepository
  ) {}

  /**
   * Returns all tasks.
   * ! Limit: 100
   */
  async listAll(): Promise<Task[]> {
    const found = this.taskRepository.find({ take: 100 })

    if (!found) {
      throw new NotFoundException()
    }

    return found
  }

  /**
   * Finds a task by it's id.
   * @param taskId The id to search for
   */
  async findTaskById(taskId: Task['taskId']): Promise<Task> {
    const found = await this.taskRepository.findOne(taskId)

    if (!found) {
      throw new NotFoundException()
    }

    return found
  }

  /**
   * Creates a new task in the service.
   * @param task The new task to add
   */
  async createTask(task: CreateTaskDto): Promise<Task> {
    return this.taskRepository
      .create({
        title: task.title,
        description: task.description ?? '',
      })
      .save()
  }

  /**
   * Finds a task by it's id an removes it.
   * Returns the deleted task
   * @param taskId The id to search for
   * @returns Copy of the deleted task
   */
  async deleteTask(taskId: Task['taskId']): Promise<Task> {
    const theTask = await this.findTaskById(taskId)
    await theTask.remove()
    return theTask
  }

  /**
   * Update zero or more values of a task
   * @param taskId The id to search for
   * @param newValues A partial task containing the new values
   */
  async updateTask(
    taskId: Task['taskId'],
    newValues: UpdateTaskDto
  ): Promise<Task> {
    const theTask = await this.findTaskById(taskId)
    if (newValues.description) {
      theTask.description = newValues.description
    }
    if (newValues.status) {
      theTask.status = newValues.status
    }
    if (newValues.title) {
      theTask.title = newValues.title
    }
    return theTask.save()
  }
}
