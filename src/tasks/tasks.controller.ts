import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Patch,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common'

import { TasksService } from './tasks.service'
import { Task } from './task.entity'

import { CreateTaskDto } from './dto/create-task.dto'
import { UpdateTaskDto } from './dto/update-task.dto'

@Controller('/tasks')
export class TasksController {
  constructor(private readonly taskService: TasksService) {}

  @Get()
  async listAll(): Promise<Task[]> {
    return this.taskService.listAll()
  }

  @Get('/:taskId')
  @UsePipes(ValidationPipe)
  async findTaskById(@Param('taskId') taskId: Task['taskId']): Promise<Task> {
    return this.taskService.findTaskById(taskId)
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createTask(@Body() body: CreateTaskDto): Promise<Task> {
    return this.taskService.createTask(body)
  }

  @Delete('/:taskId')
  @UsePipes(ValidationPipe)
  async deleteTask(@Param('taskId') taskId: Task['taskId']): Promise<Task> {
    return this.taskService.deleteTask(taskId)
  }

  @Patch('/:taskId')
  @UsePipes(ValidationPipe)
  updateTask(
    @Param('taskId') taskId: Task['taskId'],
    @Body() newValues: UpdateTaskDto
  ): Promise<Task> {
    return this.taskService.updateTask(taskId, newValues)
  }
}
