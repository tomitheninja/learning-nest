import { Injectable } from '@nestjs/common'
import { IsString, IsOptional, IsEnum } from 'class-validator'

import { ITask } from '../task.interface'
import { TaskStatus } from '../task-status.enum'

type ICreateTaskDto = Partial<Omit<ITask, 'id'>> & { id?: undefined }

@Injectable()
export class UpdateTaskDto implements ICreateTaskDto {
  @IsOptional()
  @IsString()
  public title?: ITask['title']

  @IsOptional()
  @IsString()
  public description?: ITask['description']

  @IsOptional()
  @IsEnum(TaskStatus)
  public status?: ITask['status']
}
