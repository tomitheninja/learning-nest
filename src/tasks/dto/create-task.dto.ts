import { Injectable } from '@nestjs/common'
import { IsNotEmpty, IsString, IsOptional } from 'class-validator'

import { ITask } from '../task.interface'

type ICreateTaskDto = Pick<ITask, 'title'> & Partial<Pick<ITask, 'description'>>

@Injectable()
export class CreateTaskDto implements ICreateTaskDto {
  @IsNotEmpty()
  @IsString()
  public title!: ITask['title']

  @IsOptional()
  @IsString()
  public description?: ITask['description']
}
