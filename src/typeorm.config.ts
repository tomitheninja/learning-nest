import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { Task } from './tasks/task.entity'
import { User } from './auth/user.entity'

// TODO: Load from env
export const typeOrmModuleConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: '192.168.10.89',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'learning-nest',
  entities: [Task, User],
  synchronize: true,
}
