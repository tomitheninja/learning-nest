import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { join } from 'path'

import { typeOrmModuleConfig } from '../typeorm.config'

import { AppController } from './app.controller'
import { AppService } from './app.service'

import { TasksModule } from '../tasks/tasks.module'

import { AuthModule } from 'src/auth/auth.module'

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: join(__dirname, '../../.env') }),
    TypeOrmModule.forRoot(typeOrmModuleConfig),
    TasksModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
