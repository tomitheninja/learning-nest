import { Injectable } from '@nestjs/common'
import {
  IsNotEmpty,
  IsString,
  MinLength,
  MaxLength,
  Matches,
} from 'class-validator'

const commonPassword = /^((?!(password|123|secret|qwert|iloveyou|000|111|222|333|444|555|666|777|888|999)).)*$/i

@Injectable()
export class AuthCredentialsDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(25)
  username!: string

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @Matches(/[a-z]/, {
    message: 'Password should contain at least one lowercase character',
  })
  @Matches(/[A-Z]/, {
    message: 'Password should contain at least one uppercase character',
  })
  @Matches(/[0-9]/, {
    message: 'Password should contain at least one digit',
  })
  @Matches(/[^a-z0-9]/i, {
    message: 'Password should contain at least one special character',
  })
  @Matches(commonPassword, {
    message: 'Password should not contain a common pattern (eg.: 123)',
  })
  password!: string
}
